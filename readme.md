# React app setup on wordpress

There are a lot of things happened until you get here. I will try to give basic idea about using `@wordpress/data` package to manage the application state for plugin.

You can reference the [Redux](https://redux.js.org/) documentation as the module itself  is built on [Redux](https://redux.js.org/) and wordpress documentation [Block Editor Handbook](https://developer.wordpress.org/block-editor/packages/packages-data/) which are the best way to know more in depth.

We are talking about a sample app here i.e `YOURAPP`

## What you will (must :D) learn?
### 1. Registering store for `YOURAPP` using `registerStore`?

> Okay! Let's say you get to know about or someone told you `Redux`, `@wordpress/data` package/module etc can be used to manage react application state and you're thinking how? That's where this documentation comes to play and tells you about `@wordpress/data` module, and how to use it on `YOURAPP` application.

>The `@wordpress/data` comes with a function `registerStore` which is the one of the ways to register store for the application. There are (maybe) others, I don't know :D.

>We have to feed the function `registerStore` with 2 arguments to let it know and add up functionlity to the store as per `YOURAPP` requirements.

>Let's talk about what we have to feed the function `registerStore`. The function takes a `name` to identify the store as first argument and object with values where you design your store. Its better to spent more time on second parameter which later on speed up your developing process.

```javascript
// app/store.js
import {registerStore} from '@wordpress/data' //  imported the function

/**
 * Using registerStore function to register 
 * store for [YOURAPP]
 */
registerStore('YOURAPP', {
    // We will work on this later.
})
```
The `registerStore` function just got the application's store name which is not enough to register. 
Now let's design the store as an `object` and pass it to the registerstore.
The object can have these properties:

- `reducer` Simply accepts previous __state__ and __action__ as arguments, return updated __state__ value which will be the state of the application. It is the minimum requirement to pass on object.

show me example:

```javascript
// app/store.js
// This is the second parameter object for registerStore function.
{
    /**
     * This is the reducer which accepts state and action and updates state as
     * defined under action.type
     */
    reducer: function(state, action){
        switch(action.type) { // looks on action.type.
            case 'SET_PRICE': 
            return {
                ...state, // used spread operator.
                price: action.price
            }
        }
    }
}
```
- `actions` is an object of the __functions__ which returns the __action__. The functions are known as [action creators](https://redux.js.org/glossary#action-creator) as they creates __action__ __object__ to dispatch to the above register reducer. If you want to update __state__ you have to dispatch the __action__ and reducer will update application __state__

Show me example:
```javascript
// app/store.js
// This is the second parameter object for registerStore function.
{
    /**
     * This is the reducer which accepts state and action and updates state as
     * defined under action.type
     */
    reducer: function(state, action){
        switch(action.type) { // looks on action.type.
            case 'SET_PRICE': 
                return {
                    ...state, // used spread operator.
                    price: action.price
                }
            // add cases for the actions when you add more actions.
            case default:
                break;
        }
    },
    actions: {
        setPrice: function(price) { // you are going to dispatch this action to update price on the state.
            return {
                type: 'SET_PRICE', //  this is what reducer looks to update state.
                price: price
            }
        }
        // can be add more action creators to your comfort.
    }
}
```
- `selectors` are set of __functions__ which can get you the data you wanted from __state__, depends how you programmed each selector functions. The __selector__ is called with the __state__, and additonal __arguments__ you fed.

Show me example:
```javascript
// app/store.js
// This is the second parameter object for registerStore function.
{
    /**
     * This is the reducer which accepts state and action and updates state as
     * defined under action.type
     */
    reducer: function(state, action){
        switch(action.type) { // looks on action.type.
            case 'SET_PRICE': 
                return {
                    ...state, // used spread operator.
                    price: action.price
                }
            // ... add cases for the actions when you add more actions.
            case default:
                break;
        }
    },
    actions: {
        setPrice: function(price) { // you are going to dispatch this action to update price on the state.
            return {
                type: 'SET_PRICE', //  this is what reducer looks to update state.
                price: price
            }
        }
        // ... can be add more action creators to your comfort.
    },
    selectors: { 
        getState: function(state) { // this selector will get you whole state.
            return state
        },
        getPrice: function(state, ...args) { // This selector gets you state.price only.
            return state.price
        },
        // ... Add as much selectors as much your app required.
    }
}
```
- `resolvers`
- `controls`
