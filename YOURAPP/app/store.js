import { registerStore } from '@wordpress/data' //  imported the function

/**
 * Using registerStore function to register 
 * store for [YOURAPP]
 */
registerStore(
    'YOURAPP',
    {
        /**
         * This is the reducer which accepts state and action and updates state as
         * defined under action.type
         */
        reducer: function (state, action) {
            switch (action.type) { // looks on action.type.
                case 'SET_PRICE':
                    return {
                        ...state, // used spread operator.
                        price: action.price
                    }
                // ... add cases for the actions when you add more actions.
                default:
                    break;
            }
        },
        actions: {
            setPrice: function (price) { // you are going to dispatch this action to update price on the state.
                return {
                    type: 'SET_PRICE', //  this is what reducer looks to update state.
                    price: price
                }
            }
            // ... can be add more action creators to your comfort.
        },
        selectors: {
            getState: function (state) { // this selector will get you whole state.
                return state
            },
            getPrice: function (state, ...args) { // This selector gets you state.price only.
                return state.price
            },
            // ... Add as much selectors as much your app required.
        }
    }
)